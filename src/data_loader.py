import pandas as pd
import os
from pathlib import Path
from topic_words import topic_words
import de_core_news_sm
from spacy.attrs import ORTH

nlp = de_core_news_sm.load()
nlp.tokenizer.add_special_case('<TOPIC_MASK>', [{ORTH: "<TOPIC_MASK>"}])
data_location = os.path.join(str(Path(__file__).parent.parent), 'data')


class DataLoader(object):
    def __init__(self):
        self.original_data_path = os.path.join(data_location, 'Kai_RF_data_merged.csv')
        self.training_data_path = os.path.join(data_location, 'train.csv')
        self.holdout_data_path = os.path.join(data_location, 'holdout.csv')

        self.training_data_LIWC_path = os.path.join(data_location, 'train_LIWC.csv')
        self.holdout_data_LIWC_path = os.path.join(data_location, 'holdout_LIWC.csv')
        self.score_100W_training_data_path = os.path.join(data_location, 'train_100W.csv')
        self.score_100W_holdout_data_path = os.path.join(data_location, 'holdout_100W.csv')
        self.train_data = self.get_training_data()
        self.holdout_data = self.get_holdout_data()
        self.features = []
        for lab, words in topic_words.items():
            self.features.extend([w.lower() for w in words])

        if 'masked' not in self.train_data.columns:
            self.train_data['masked'] = self.get_topic_masked_or_deleted_data(train=True, delete=False)
            self.train_data['deleted'] = self.get_topic_masked_or_deleted_data(train=True, delete=True)
            self.holdout_data['masked'] = self.get_topic_masked_or_deleted_data(train=False, delete=False)
            self.holdout_data['deleted'] = self.get_topic_masked_or_deleted_data(train=False, delete=True)
            self.train_data.to_csv(data_location + '/train.csv')
            self.holdout_data.to_csv(data_location + '/holdout.csv')

        self.print_data_stats()

    def get_filtered_data(self, path):
        if path.endswith('.xlsx'):
            data = pd.read_excel(path)
        else:
            data = pd.read_csv(path)
        data = data[data.focus != 'unknown']
        lab = [l if l in [1, 2] else l - 2 for l in data['focus_id']]
        data['focus_id'] = lab
        data['label'] = data['focus_id'] - 1
        data['len'] = data.text.apply(str).apply(len)
        data = data[data.len > 3]
        data_classes = ['study', 'travel', 'other']

        if 'annotated_topic' in data.columns:
            data['annotated_label'] = data['annotated_topic'].apply(data_classes.index)

        if 'lexicon_annotated_topic' in data.columns:
            data['lexicon_label'] = data['lexicon_annotated_topic'].apply(data_classes.index)

        cols = [c for c in data.columns if c not in ['len', 'nan']]
        return data[cols]

    def get_original_data(self):
        return self.get_filtered_data(self.original_data_path)

    def get_training_data(self):
        return self.get_filtered_data(self.training_data_path)

    def get_holdout_data(self):
        return self.get_filtered_data(self.holdout_data_path)

    def print_data_stats(self):
        print("Training data: {}".format(len(self.get_training_data())))
        print("Holdout data: {}".format(len(self.get_holdout_data())))

    def get_topic_masked_or_deleted_data(self, train, delete=False):

        data = self.train_data if train else self.holdout_data
        docs = list(data.text)
        masked_training_data = []

        if delete and 'masked' in data.columns:
            print('Masked data found, deleting mask')
            docs = list(data.masked)
            return [d.replace('<TOPIC_MASK>', '') for d in docs]

        print('total features to be masked: ', len(self.features))
        print('masking topics in data : train=', train)
        for text in docs:
            for word in self.features:
                toks = [t.text for t in nlp(text)]
                if word in toks:
                    toks[toks.index(word)] = '<TOPIC_MASK>'
                elif word.lower() in toks:
                    toks[toks.index(word.lower())] = '<TOPIC_MASK>'
                else:
                    # match partial words
                    partial = [tok for tok in toks if word.lower() in tok.lower()]
                    for part in partial:
                        toks[toks.index(part)] = '<TOPIC_MASK>'

                if delete:
                    text = " ".join([t for t in toks if t != '<TOPIC_MASK>'])
                else:
                    text = " ".join(toks)
            masked_training_data.append(text)
        print('done')
        return masked_training_data


if __name__ == "__main__":
    from bert_classifier import output_location

    loader = DataLoader()
    loader.train_data.to_csv(output_location + 'train.csv')
    loader.holdout_data.to_csv(output_location + 'holdout.csv')
