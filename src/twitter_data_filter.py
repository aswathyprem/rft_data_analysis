import pandas as pd
import os
from collections import Counter
import re

data_path = '/projekte/tcl/seat/twitterdata/'
# out_path = '/home/users0/veluthay/PhD/rft_data_analysis/output/'
out_path = '../annotations/'
tw_data_path = '../annotations/twitter_data'

emo_list = [" klar", " wüt", " angewider", " betrüb", " erstaun", " erschrock", " bewunder", " begeister",
            " froh", " bereit", " verärger", " ablehn", " traurig", " überrasch", " ängst", " vertrau",
            " akzeptier", " gelass", " neugierig", " gereiz", " gelangweil", " nachdenk", " verwirr",
            " besorg", " stolz", " aufmerksam", " klar", " optimist", " verlieb", " streitlust", " hass",
            " bereund", " enttäusch", " ehrfürchtig", " fügsam", " scham"]


def check_hashtags_percentage(text):
    words = text.split()
    hashtags = [w for w in words if w[0] == '#']
    return len(hashtags) / len(words) < 0.2


def check_url(text):
    return 'http' not in text


def fetch_keyword_data(key_word, files):
    all_data = pd.DataFrame()
    key_words = ["Ich ", "Wir "]

    for f in files:
        data = pd.DataFrame()
        ids = []
        texts = []
        dates = []
        try:
            with bz2.open(data_path + f, 'rt') as bzfile:
                for line in bzfile:
                    columns = line.split('\t')
                    if len(columns) > 8:
                        dates.append(columns[2])
                        ids.append(columns[3])
                        texts.append(columns[8])

        except:
            print('error reading file : ', f)
            pass
        data['id'] = ids
        data['tweet'] = texts
        data['date'] = dates
        print('Total tweets in {} = {}'.format(f, len(data)))
        print(data.columns)
        data = data[data['tweet'].str.contains('|'.join(key_words), na=False)]
        print('Total tweets in {} with keywords Ich or Wir = {}'.format(f, len(data)))
        if len(data) == 0:
            continue
        data = data[data['tweet'].apply(check_hashtags_percentage)]

        if len(data) == 0:
            continue
        data = data[data['tweet'].apply(check_url)]
        all_data = data if len(all_data) == 0 else pd.concat([all_data, data])
        print('-----extracted till now = {} -----'.format(len(all_data)))
    all_data.to_csv(out_path + '_2016-2019.csv')


def clean_text(text):
    # remove string
    text = text.replace("[NEWLINE]", "")

    # remove username
    words = text.split()
    words = [w for w in words if '@' not in w]
    # remove hashtags in the end and just the hash otherwise
    while "#" in words[-1]:
        words = words[:-1]
        if not words:
            break
    for i, word in enumerate(words):
        words[i] = word.replace("#", '')
    return " ".join(words)


def apply_filtering():
    files = [f for f in os.listdir(tw_data_path) if '2016' in f]
    for f in files:
        data = pd.read_csv(tw_data_path + f)
        data['filtered'] = data['tweet'].apply(clean_text)
        data = data[data.filtered.apply(lambda x: len(x.split()) > 10)]
        data['emo'] = data['tweet'].apply(tag_emotion)
        data.to_csv(out_path + f)


def tag_emotion(text):
    for emo in emo_list:
        if emo in text.lower():
            return emo
    return False


def get_data_for_annotation():
    files = [f for f in os.listdir(tw_data_path) if '2016' in f]
    all_data = pd.DataFrame()
    all_data_len = 0
    for f in files:
        data = pd.read_csv(tw_data_path + f)
        all_data_len += len(data)
        all_data = pd.concat((all_data, data))

    print(len(all_data))
    all_data['filtered'] = all_data['filtered'].apply(str)
    all_data = all_data[~all_data['filtered'].str.contains('corona', flags=re.IGNORECASE)]
    print(len(all_data))

    subset_data = all_data.sample(n=100000, random_state=42)
    subset_data = subset_data[subset_data['emo'] != "False"]
    print(len(subset_data))
    print(Counter(subset_data.emo))

    #annotation_data = stratified_sample_df(subset_data, 'emo', 42).reset_index()
    annotation_df = subset_data.sample(1800, random_state=42)
    annotation_df = annotation_df[['emo', 'filtered']]
    print(Counter(annotation_df.emo))

    annotation_df[:100].to_excel(out_path + 'annotation_training.xlsx')
    annotation_df[100:200].to_excel(out_path + 'annotation_training_phase2.xlsx')
    annotation_df[200:300].to_excel(out_path + 'annotation_training_phase3.xlsx')
    annotation_df[300:].to_excel(out_path + 'annotate_final.xlsx')


def stratified_sample_df(df, col, n_samples):
    # n = min(n_samples, df[col].value_counts().min())
    df_ = df.groupby(col).apply(lambda x: x.sample(frac=0.5, random_state=42))
    df_.index = df_.index.droplevel(0)
    return df_


if __name__ == '__main__':
    apply_filtering()
    get_data_for_annotation()
