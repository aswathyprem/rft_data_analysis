import os
import torch
import random
import time
import numpy as np
import pandas as pd
import datetime
from pathlib import Path
import seaborn as sns
from collections import Counter
import matplotlib.pyplot as plt
from lime.lime_text import LimeTextExplainer
from baseline import german_stop_words
from results import EvalResult
from data_loader import DataLoader as dl
from sklearn.metrics import precision_recall_fscore_support
from bert4seqclassification_modified import BertForSequenceClassification as AdversarialBertForSequenceClassification
from transformers import BertTokenizerFast, AdamW, get_linear_schedule_with_warmup, BertForSequenceClassification
from torch.utils.data import DataLoader, TensorDataset, random_split

output_location = os.path.join(str(Path(__file__).parent.parent), 'output')
# model_location = os.path.join(str(Path(__file__).parent.parent), 'models')
model_location = '/mount/arbeitsdaten/tcl/Users/aswathy/models/'


class BertClassifier(object):

    def __init__(self, **kwargs):

        model_name = kwargs['model_name']
        train_data = kwargs['train_data']
        holdout_data = kwargs['holdout_data']
        ref = kwargs['ref']
        self.adversarial = kwargs['adversarial'] if 'adversarial' in kwargs else False
        self.reverse = kwargs['reverse_flag'] if 'reverse_flag' in kwargs else False
        self.alpha = kwargs['alpha'] if 'alpha' in kwargs else False
        self.masked = kwargs['masked'] if 'masked' in kwargs else False
        self.deleted = kwargs['deleted'] if 'deleted' in kwargs else False

        self.epochs = kwargs['epochs'] if 'epochs' in kwargs else False
        self.learning_rate = kwargs['learning_rate'] if 'learning_rate' in kwargs else 1e-5
        self.batch_size = kwargs['batch_size'] if 'batch_size' in kwargs else 4

        self.mask_topic_in_test_data = kwargs[
            'mask_topic_in_test_data'] if 'mask_topic_in_test_data' in kwargs else False
        self.delete_topic_in_test_data = kwargs[
            'delete_topic_in_test_data'] if 'delete_topic_in_test_data' in kwargs else False

        self.meta = kwargs
        self.meta['topic_source'] = kwargs['y_col2']
        if not self.epochs:
            raise AttributeError
        self.ref = ref
        self.text_col = kwargs['x_col']
        self.label_col = kwargs['y_col1']
        self.label_col_2 = kwargs['y_col2']
        self.model_name = model_name  # "bert-base-german-cased"
        self.train_data = train_data
        self.holdout_data = holdout_data

        self.labels = sorted(list(set(train_data[self.label_col])))
        self.tokenizer = BertTokenizerFast.from_pretrained(model_name)
        self.tokenizer.add_tokens(['<TOPIC_MASK>'])
        self.model = BertForSequenceClassification.from_pretrained(model_name, num_labels=len(self.labels),
                                                                   output_attentions=True,
                                                                   output_hidden_states=True)
        # print(self.model)
        # print(self.adversarial)

        if self.masked:
            assert not self.adversarial
            print("==============TOPIC MASKED SETTING=============")
            if not self.mask_topic_in_test_data:
                self.holdout_data['masked'] = self.holdout_data['text']
            self.text_col = 'masked'
        if self.deleted:
            print("==============TOPIC DELETED SETTING=============")
            assert not self.masked
            assert not self.adversarial
            if not self.delete_topic_in_test_data:
                self.holdout_data['deleted'] = self.holdout_data['text']
            self.text_col = 'deleted'

        if self.adversarial:
            assert y_col2 is not None
            print("==============ADVERSARIAL SETTING=============")
            self.t_labels = sorted(list(set(train_data[self.label_col_2])))
            self.model = AdversarialBertForSequenceClassification.from_pretrained(model_name,
                                                                                  num_labels=len(self.labels),
                                                                                  output_attentions=True,
                                                                                  output_hidden_states=True,
                                                                                  num_topic_labels=len(self.t_labels))
        # params
        self.output_model_dir = os.path.join(str(Path(__file__).parent.parent), 'models')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.max_length = self.get_max_length(self.train_data.text)
        self.train_dataloader, self.validation_dataloader = self.prepare_dataloaders(self.text_col, self.label_col,
                                                                                     self.label_col_2)
        self.total_steps = len(self.train_dataloader) * self.epochs
        self.optimizer = self.get_optimizer()
        self.scheduler = self.get_scheduler()

    def get_optimizer(self):
        optimizer = AdamW(self.model.parameters(),
                          lr=self.learning_rate,
                          eps=1e-8
                          )
        return optimizer

    def get_scheduler(self):
        scheduler = get_linear_schedule_with_warmup(self.optimizer,
                                                    num_warmup_steps=0,
                                                    num_training_steps=self.total_steps)
        return scheduler

    def flat_accuracy(self, preds, labels):
        pred_flat = np.argmax(preds, axis=1).flatten()
        labels_flat = labels.flatten()
        acc = np.sum(pred_flat == labels_flat) / len(labels_flat)
        _, _, f1_macro, _ = precision_recall_fscore_support(labels_flat, pred_flat, average='macro')
        return acc, f1_macro

    def format_time(self, elapsed):
        # Round to the nearest second.
        elapsed_rounded = int(round((elapsed)))

        # Format as hh:mm:ss
        return str(datetime.timedelta(seconds=elapsed_rounded))

    def get_max_length(self, X):
        max_len = 0
        for sent in X:
            input_ids = self.tokenizer.encode(sent, add_special_tokens=True)
            max_len = max(max_len, len(input_ids))
        print('Max sentence length: ', max_len)
        return max_len

    def get_vectors_for_text(self, sent):
        encoded_dict = self.tokenizer.encode_plus(
            sent,
            add_special_tokens=True,
            max_length=self.max_length,
            truncation=True,
            pad_to_max_length=True,
            return_attention_mask=True,
            return_tensors='pt',
        )
        return encoded_dict['input_ids'], encoded_dict['attention_mask']

    def get_encoded_inputs(self, sentences, labels):
        input_ids = []
        attention_masks = []
        for sent in sentences:
            inp_id, attn_mask = self.get_vectors_for_text(sent)
            input_ids.append(inp_id)
            attention_masks.append(attn_mask)

        # Convert the lists into tensors.
        input_ids = torch.cat(input_ids, dim=0)
        attention_masks = torch.cat(attention_masks, dim=0)
        labels = torch.tensor(labels)
        return input_ids, attention_masks, labels

    def prepare_dataloaders(self, x_col, y_col, y_col2):
        X = self.train_data[x_col]
        y = list(self.train_data[y_col])
        input_ids, attention_masks, labels = self.get_encoded_inputs(X, y)

        if self.adversarial:
            topic_labels = torch.tensor(list(self.train_data[y_col2]))
            dataset = TensorDataset(input_ids, attention_masks, labels, topic_labels)
        else:
            dataset = TensorDataset(input_ids, attention_masks, labels)

        train_size = int(0.9 * len(dataset))
        val_size = len(dataset) - train_size
        train_dataset, val_dataset = random_split(dataset, [train_size, val_size],
                                                  generator=torch.Generator().manual_seed(42))
        batch_size = self.batch_size
        train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=batch_size)
        validation_dataloader = DataLoader(val_dataset, shuffle=False, batch_size=batch_size)
        return train_dataloader, validation_dataloader

    def train_model(self):
        seed_val = 42
        random.seed(seed_val)
        np.random.seed(seed_val)
        torch.manual_seed(seed_val)
        torch.cuda.manual_seed_all(seed_val)
        training_stats = []
        total_t0 = time.time()
        for epoch_i in range(0, self.epochs):

            print("")
            print('======== Epoch {:} / {:} ========'.format(epoch_i + 1, self.epochs))
            print('Training...')
            self.model.cuda()
            t0 = time.time()
            total_train_loss = 0
            total_topic_loss = 0
            total_rft_loss = 0
            self.model.train()
            for step, batch in enumerate(self.train_dataloader):
                if not self.alpha:
                    p = float(step + epoch_i * len(self.train_dataloader)) / (self.epochs * len(self.train_dataloader))
                    alpha = 2. / (1. + np.exp(-10 * p)) - 1
                else:
                    alpha = self.alpha
                if step % 300 == 0 and not step == 0:
                    elapsed = self.format_time(time.time() - t0)
                    print('  Batch {:>5,}  of  {:>5,}.    Elapsed: {:}.'.format(step, len(self.train_dataloader),
                                                                                elapsed))
                labels = batch[2].to(self.device)
                inputs = {"input_ids": batch[0].to(self.device),
                          "token_type_ids": None,
                          "attention_mask": batch[1].to(self.device),
                          "labels": labels}

                if self.adversarial:
                    t_labels = batch[3].to(self.device)
                    inputs["topic_labels"] = t_labels
                    inputs["alpha"] = alpha
                    inputs["reverse"] = self.reverse
                self.model.zero_grad()
                output = self.model(**inputs)
                loss = output.loss
                logits = output.logits

                if self.adversarial:
                    total_rft_loss += output.loss.item()
                    total_topic_loss += output.topic_loss.item()
                    loss = loss + output.topic_loss

                total_train_loss += loss.item()

                # Perform a backward pass to calculate the gradients.
                loss.backward()

                # Clip the norm of the gradients to 1.0.
                # This is to help prevent the "exploding gradients" problem.
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), 1.0)

                self.optimizer.step()
                self.scheduler.step()

            avg_train_loss = total_train_loss / len(self.train_dataloader)
            training_time = self.format_time(time.time() - t0)

            print("")
            print("  Average training loss: {0:.2f}".format(avg_train_loss))
            print("  Training epcoh took: {:}".format(training_time))
            avg_topic_train_loss = 0
            avg_rft_train_loss = 0
            if self.adversarial:
                avg_rft_train_loss = total_rft_loss / len(self.train_dataloader)
                print("  Average rft training loss: {0:.2f}".format(avg_rft_train_loss))
                avg_topic_train_loss = total_topic_loss / len(self.train_dataloader)
                print("  Average topic training loss: {0:.2f}".format(avg_topic_train_loss))

            # ========================================
            #               Validation
            # ========================================
            # After the completion of each training epoch, measure our performance on
            # our validation set.

            print("")
            print("Running Validation...")

            t0 = time.time()

            # Put the model in evaluation mode--the dropout layers behave differently
            # during evaluation.
            self.model.eval()

            # Tracking variables
            total_eval_accuracy = 0
            total_f1_macro = 0
            total_eval_loss = 0
            total_topic_eval_accuracy = 0
            total_topic_f1_macro = 0
            total_topic_eval_loss = 0
            nb_eval_steps = 0

            # Evaluate data for one epoch
            for batch in self.validation_dataloader:

                b_input_ids = batch[0].to(self.device)
                b_input_mask = batch[1].to(self.device)
                b_labels = batch[2].to(self.device)
                inputs = {"input_ids": b_input_ids,
                          "token_type_ids": None,
                          "attention_mask": b_input_mask,
                          "labels": b_labels}

                if self.adversarial:
                    t_labels = batch[3].to(self.device)
                    inputs["topic_labels"] = t_labels
                    inputs["alpha"] = alpha
                    inputs["reverse"] = self.reverse

                with torch.no_grad():
                    output = self.model(**inputs)
                    loss = output.loss
                    logits = output.logits

                # Accumulate the validation loss.
                total_eval_loss += loss.item()

                # Move logits and labels to CPU
                logits = logits.detach().cpu().numpy()
                label_ids = b_labels.to('cpu').numpy()

                acc, f1_macro = self.flat_accuracy(logits, label_ids)
                total_eval_accuracy += acc
                total_f1_macro += f1_macro

                if self.adversarial:
                    total_topic_eval_loss += output.topic_loss.item()
                    topic_logits = output.topic_logits.detach().cpu().numpy()
                    topic_label_ids = t_labels.to('cpu').numpy()
                    acc, f1_macro = self.flat_accuracy(topic_logits, topic_label_ids)
                    total_topic_eval_accuracy += acc
                    total_topic_f1_macro += f1_macro

            # Report the final accuracy for this validation run.
            avg_val_accuracy = total_eval_accuracy / len(self.validation_dataloader)
            print("  Accuracy: {0:.2f}".format(avg_val_accuracy))

            avg_f1_score = total_f1_macro / len(self.validation_dataloader)
            print("  F1 score: {0:.2f}".format(avg_f1_score))

            # Calculate the average loss over all of the batches.
            avg_val_loss = total_eval_loss / len(self.validation_dataloader)
            print("  Validation Loss: {0:.2f}".format(avg_val_loss))

            avg_topic_val_accuracy = 0
            avg_topic_f1_score = 0
            avg_topic_val_loss = 0
            if self.adversarial:
                # Report the final accuracy for this validation run.
                avg_topic_val_accuracy = total_topic_eval_accuracy / len(self.validation_dataloader)
                print(" Topic Accuracy: {0:.2f}".format(avg_topic_val_accuracy))

                # Calculate the average loss over all of the batches.
                avg_topic_val_loss = total_topic_eval_loss / len(self.validation_dataloader)
                print(" Topic Validation Loss: {0:.2f}".format(avg_topic_val_loss))

                # Calculate the average loss over all of the batches.
                avg_topic_f1_score = total_topic_f1_macro / len(self.validation_dataloader)
                print(" Topic F1 score: {0:.2f}".format(avg_topic_f1_score))

            # Measure how long the validation run took.
            validation_time = self.format_time(time.time() - t0)
            print("  Validation took: {:}".format(validation_time))

            # Record all statistics from this epoch.
            training_stats.append(
                {
                    'epoch': epoch_i + 1,
                    'Training Loss': avg_train_loss,
                    'Topic Training Loss': avg_topic_train_loss,
                    'RFT Training Loss': avg_rft_train_loss,
                    'Valid. Loss': avg_val_loss,  # RFT loss
                    'Valid. Accur.': avg_val_accuracy,  # RFT acc
                    'Valid. F1.': avg_f1_score,  # RFT f1
                    'Topic Valid. Loss': avg_topic_val_loss,  # topic loss
                    'Topic Valid. Accur.': avg_topic_val_accuracy,  # topic acc
                    'Topic Valid. F1.': avg_topic_f1_score,  # topic f1
                    'Training Time': training_time,
                    'Validation Time': validation_time
                }
            )

        print("")
        print("Training complete!")
        print("Total training took {:} (h:mm:ss)".format(self.format_time(time.time() - total_t0)))
        result = EvalResult(self.ref, input=list(self.train_data.text), y_true=[], y_pred=[],
                            mode='train-test',
                            train_stats=training_stats, meta=self.meta)
        return self.model, self.tokenizer, result

    def save_model(self, model_save_name):
        output_dir = os.path.join(self.output_model_dir, model_save_name)
        # Create output directory if needed
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        print("Saving model to %s" % output_dir)
        model_to_save = self.model.module if hasattr(self.model,
                                                     'module') else self.model  # Take care of distributed/parallel training
        model_to_save.save_pretrained(output_dir)
        self.tokenizer.save_pretrained(output_dir)

    @staticmethod
    def plot_stats(training_stats, mode='loss'):
        pd.set_option('precision', 2)
        df_stats = pd.DataFrame(data=training_stats)
        df_stats = df_stats.set_index('epoch')
        sns.set(style='darkgrid')
        sns.set(font_scale=1.5)
        plt.rcParams["figure.figsize"] = (12, 6)
        if mode == 'loss':
            plt.plot(df_stats['Training Loss'], 'b-o', label="Training")
            plt.plot(df_stats['Topic Training Loss'], 'b-o', label="Training")
            plt.plot(df_stats['RFT Training Loss'], 'b-o', label="Training")
            plt.plot(df_stats['Topic Valid. Loss'], 'r-o', label="Topic_Validation")
            plt.plot(df_stats['Valid. Loss'], 'g-o', label="Validation")
            plt.title("Training & Validation Loss")
            plt.ylabel("Loss")
        else:
            plt.plot(df_stats['Valid. Accur.'], 'b-o', label="Topic Accuracy.")
            plt.plot(df_stats['Topic Valid. Accur.'], 'g-o', label="RFT Accuracy.")
            plt.title("RFT & Topic Validation Accuracy")
            plt.ylabel("Accuracy")

        plt.xlabel("Epoch")
        plt.legend()
        plt.xticks([1, 2, 3, 4, 5, 6])
        plt.show()

    def get_logits(self, sent):
        encoded_dict = self.tokenizer.encode_plus(
            sent,
            add_special_tokens=True,
            max_length=self.max_length,
            truncation=True,
            pad_to_max_length=True,
            return_attention_mask=True,
            return_tensors='pt',
        )

        input_ids = encoded_dict["input_ids"].detach().clone().to(self.device)
        attention_mask = encoded_dict["attention_mask"].detach().clone().to(self.device)
        output = self.model(input_ids, attention_mask)
        # print(vars(output).keys())
        # print(self.adversarial)
        return output.logits if not self.adversarial else (output.logits, output.topic_logits)

    def get_predictions(self, sent):
        if not self.adversarial:
            _, prediction = torch.max(self.get_logits(sent), dim=1)
            return prediction.tolist()[0]
        else:
            logits, topic_logits = self.get_logits(sent)
            _, prediction = torch.max(logits, dim=1)
            _, topic_prediction = torch.max(topic_logits, dim=1)
            return prediction.tolist()[0], topic_prediction.tolist()[0]

    def predict_proba(self, samples):
        probs = []
        for i, sent in enumerate(samples):
            softmax = torch.nn.functional.softmax(self.get_logits(sent), dim=1)
            probability = softmax.tolist()[0]
            probs.append(probability)
        return np.array(probs)

    def load_model(self, model_name):
        output_dir = os.path.join(self.output_model_dir, model_name)
        model = BertForSequenceClassification.from_pretrained(output_dir)
        tokenizer = BertTokenizerFast.from_pretrained(output_dir)
        if self.device == 'cuda':
            model.cuda()
        return model, tokenizer

    def evaluate(self, X_test, y_test, y_topic_test=None):
        print('evaluating the model')
        predictions = []
        topic_predictions = []
        self.model.eval()
        if self.adversarial:
            predictions = [self.get_predictions(text)[0] for text in X_test]
            topic_predictions = [self.get_predictions(text)[1] for text in X_test]
        else:
            predictions = [self.get_predictions(text) for text in X_test]
        return EvalResult(self.ref, input=list(X_test), y_true=y_test, y_pred=predictions,
                          topic_pred=topic_predictions, topic_true=y_topic_test,
                          mode='holdout')

    def explain_predictions(self, incorrect_predictions, correct_predictions):
        print('explaining  evaluation')
        features = []
        class_names = ['promotion', 'prevention']
        explainer = LimeTextExplainer(class_names=class_names)

        for i in incorrect_predictions + correct_predictions:
            exp = explainer.explain_instance(i['text'], self.predict_proba, num_features=6, num_samples=1000)
            features.extend([f[0] for f in exp.as_list() if f[0].lower() not in german_stop_words])
        return Counter(features)

    def apply_bert_modelling(self):

        results = []
        # train model
        model, tokenizer, train_result = self.train_model()
        results.append(train_result)

        # save model
        model_path = model_location + '/_' + self.ref
        self.save_model(model_path)

        # evaluate model
        if self.adversarial:
            eval_result = self.evaluate(self.holdout_data[self.text_col],
                                        self.holdout_data[self.label_col],
                                        self.holdout_data[self.label_col_2])
        else:
            eval_result = self.evaluate(self.holdout_data[self.text_col], self.holdout_data[self.label_col])

        # explain model
        # eval_result.feats_counter = self.explain_predictions(eval_result.false_pred, eval_result.true_pred)

        print(eval_result.classification_report)
        results.append(eval_result)
        return results


if __name__ == "__main__":
    dl = dl()
    inputs = {"model_name": "bert-base-german-cased",
              "train_data": dl.get_training_data(),
              "holdout_data": dl.get_holdout_data(),
              "x_col": "text",
              "y_col1": "label",
              "ref": "masked",
              "deleted": True,
              "epochs": 4,
              "delete_topic_in_test_data": True
              }
    b = BertClassifier(**inputs)
    print(list(b.train_data[b.text_col][:10]))
    print(list(b.holdout_data[b.text_col][:10]))
