from baseline import WB_models
from bert_classifier import BertClassifier
from data_loader import DataLoader as dl
import pickle
import os
from pathlib import Path
import warnings
import argparse

warnings.filterwarnings("ignore")

output_path = os.path.join(str(Path(__file__).parent.parent), 'output')

if __name__ == '__main__':

    def apply_bert(inputs):
        bert_classifier = BertClassifier(**inputs)
        return bert_classifier.apply_bert_modelling()


    parser = argparse.ArgumentParser(description='Get model args')
    parser.add_argument('--model_name', type=str, default='bert-base-german-cased')
    parser.add_argument('--epochs', type=int, default=4)
    parser.add_argument('--model_reference', type=str, default=None, required=True)
    parser.add_argument('--is_masked', type=bool, default=False)
    parser.add_argument('--is_deleted', type=bool, default=False)
    parser.add_argument('--is_adversarial', type=bool, default=False)
    parser.add_argument('--alpha', type=list, default=False)
    parser.add_argument('--reverse_flag', type=bool, default=True)
    parser.add_argument('--label_col', type=str, default='label')
    parser.add_argument('--text_col', type=str, default='text')
    parser.add_argument('--topic_label_col', type=str, default='lexicon_label')
    parser.add_argument('--model_dir', default=None, type=str)
    parser.add_argument('--result_pickle', default=None, type=str, required=True)
    parser.add_argument('--do_hyperparam_optimization', type=bool, default=False)

    args = parser.parse_args()
    aggregated_res = []
    dl = dl()
    training_data = dl.train_data
    holdout_data = dl.holdout_data

    input = {
        "model_name": args.model_name,
        "train_data": training_data,
        "holdout_data": holdout_data,
        "x_col": args.text_col,
        "y_col1": args.label_col,
        "ref": args.model_reference,
        "masked": args.is_masked,
        "delete": args.is_deleted,
        "adversarial": args.is_adversarial,
        "y_col2": args.topic_label_col,
        "reverse_flag": args.reverse_flag,
        "epochs": args.epochs
    }

    if args.do_hyperparam_optimization:
        params = {
            "batch_size": [4, 16, 32],
            "learning_rate": [1e-5, 2e-5, 3e-5, 5e-5],
            "epochs": [4, 40, 400, 4000]
        }

        for k, v in params.items():
            for val in v:
                input[k] = val
                aggregated_res.append(apply_bert(input))

    else:
        aggregated_res.append(apply_bert(input))

    out_file = os.path.join(output_path, args.result_pickle)
    with open(out_file, 'wb') as f:
        pickle.dump(aggregated_res, f, pickle.HIGHEST_PROTOCOL)

#######
#
# #### Bag of words models :
# inputs = {"model_name": "bert-base-german-cased",
#           "train_data": training_data,
#           "holdout_data":holdout_data,
#           "x_col": "text",
#           "y_col1": "label",
#           "ref": "tfidf"
#           }
#
# aggregate_evaluations.extend(apply_BOW(inputs))
#
# inputs['masked'] = True
# inputs['ref'] = 'tfidf_masked'
# aggregate_evaluations.extend(apply_BOW(inputs))
#
# inputs['masked'] = False
# inputs['deleted'] = True
# inputs['ref'] = 'tfidf_deleted'
# aggregate_evaluations.extend(apply_BOW(inputs))
#


# wb_models = WB_models()
# aggregate_evaluations.extend(wb_models.get_lexicon_classification('LIWC_lexicon'))
# aggregate_evaluations.extend(wb_models.get_lexicon_classification('100W_lexicon'))
# aggregate_evaluations.extend(wb_models.apply_logistic_regression_relative_frequency('LIWC_maxent'))
# aggregate_evaluations.extend(wb_models.apply_logistic_regression_relative_frequency('100W_maxent'))
# aggregate_evaluations.extend(wb_models.apply_logistic_regeression_with_tfidf_features())
# aggregate_evaluations.extend(wb_models.apply_logistic_regeression_with_tfidf_features(delete=True))

#
# def apply_BOW(inputs):
#     bow_classifier = BOWClassifier(**inputs)
#     return bow_classifier.apply_logistic_regeression()
