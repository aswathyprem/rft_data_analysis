from abc import ABC
from typing import Any

from transformers import BertModel, BertForSequenceClassification, BertPreTrainedModel
from torch import nn
from torch.nn import CrossEntropyLoss
from torch.autograd import Function
from transformers.modeling_outputs import SequenceClassifierOutput
import torch.nn as nn
import torch


class ReverseLayerF(Function):

    @staticmethod
    def forward(ctx, x, alpha):
        ctx.alpha = alpha
        return x.view_as(x)

    @staticmethod
    def backward(ctx, grad_output):
        output = grad_output.neg() * ctx.alpha
        return output, None


class SequenceClassifierOutput(object):
    def __init__(self, loss, logits, hidden_states, attentions, topic_loss=None, topic_logits=None):
        self.loss: Optional[torch.FloatTensor] = loss
        self.logits: torch.FloatTensor = logits
        self.hidden_states: Optional[Tuple[torch.FloatTensor]] = hidden_states
        self.attentions: Optional[Tuple[torch.FloatTensor]] = attentions
        self.topic_loss: Optional[torch.FloatTensor] = topic_loss
        self.topic_logits: torch.FloatTensor = topic_logits


class BertForSequenceClassification(BertPreTrainedModel):
    def __init__(self, config, num_topic_labels=None):
        super().__init__(config)
        self.num_labels = config.num_labels
        self.bert = BertModel(config)
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
        self.classifier = nn.Linear(config.hidden_size, config.num_labels)
        self.num_topic_labels = num_topic_labels
        self.topic_classifier = nn.Linear(config.hidden_size, self.num_topic_labels)
        self.init_weights()

    def forward(
            self,
            input_ids=None,
            attention_mask=None,
            token_type_ids=None,
            position_ids=None,
            head_mask=None,
            inputs_embeds=None,
            labels=None,
            topic_labels=None,
            output_attentions=None,
            output_hidden_states=None,
            return_dict=None,
            alpha=0.5,
            reverse=False,
    ):

        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )

        pooled_output = outputs[1]

        pooled_output = self.dropout(pooled_output)
        logits = self.classifier(pooled_output)
        if not reverse:
            # without reversal
            topic_logits = self.topic_classifier(pooled_output)
        else:
            # apply layer reveresal
            reversed_pooled_output = ReverseLayerF.apply(pooled_output, alpha)
            topic_logits = self.topic_classifier(reversed_pooled_output)

        topic_loss = None
        if topic_labels is not None:
            topic_loss_fct = CrossEntropyLoss()
            topic_loss = topic_loss_fct(topic_logits.view(-1, self.num_topic_labels), topic_labels.view(-1))

        loss = None
        if labels is not None:
            if self.num_labels != 1:
                # not regression
                loss_fct = CrossEntropyLoss()
                loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))

        if not return_dict:
            output = (logits,) + outputs[2:]
            return ((loss,) + output) if loss is not None else output

        return SequenceClassifierOutput(
            loss=loss,
            logits=logits,
            hidden_states=outputs.hidden_states,
            attentions=outputs.attentions,
            topic_loss=topic_loss,
            topic_logits=topic_logits
        )
