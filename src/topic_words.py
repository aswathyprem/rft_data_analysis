from collections import Counter

topic_words = {
    'study': ['Studium', 'Studiums',
              'Kursus', 'Kurse', 'Kurs', 'Kurses',
              'Lehrgang', 'Lehrgänge', 'Lehrgaenge', 'Lehrganges', 'Lehrgangen',
              'Ausbildung', 'Ausbildungen', 'Ausbuildung', 'Seminar',
              'Praktikum', 'Praktika', 'Praktikums', 'Praktikant',
              'Noten', 'Note', 'Schule', 'Schulen',
              'Klassenstufe', 'Klassenstufen',
              'Gymnasium', 'Gymnasiums', 'Gymnasien',
              'Abitur', 'Abiturs', 'Abi', 'Abiturzeugnis', 'Abiturpruefungen', 'Abiturprüfungen',
              'Semester', 'Semesters', 'Semestern', 'Klausur', 'Klausuren', 'Klauseur',
              'Uni', 'Universität', 'Universitäten', 'Universitaet',
              'Staatsexamen', 'Staatsexamina', 'Staatsexam',
              'Pruefung', 'Prüfung', 'Pruefungen', 'Prüfungen', 'Prufungen',
              'Pruefungsergebnisse', 'Prüfungsergebnisse',
              'Studienplatz', 'Studiengang', 'Studienfact', 'Studienganges', 'Studien',
              'Studenten', 'Student',
              'Abschluss', 'Abschlusse', 'Benotung', 'Kommilitonen', 'Kommilitonin', 'Kommilitone',
              'Professor', 'Professorin', 'Professoren', 'Professorinnen', 'Prof',
              'Lehrern', 'Lehrer', 'Lehrerin', 'Lehrerinnen',
              'Lerngruppe', 'Hausarbeit',
              'Diplomarbeit', 'Masterarbeit', 'Bachelorarbeit', 'Klassenarbeit', 'Doktorarbeit',
              'Bibliothek', 'Schule',
              'Auslandsjahr', 'Auslandsstudium', 'Auslandsemester', 'Austauschprogramm',
              'Arbeitsgruppe', 'Schulregeln',
              'Vorlesung', 'Vorlesungen', 'Tutorium', 'Theorie',
              'Stipendium', 'Stipendiat', 'Stipendien',
              'Uebungsblatt', 'Uebung',
              'Physik', 'Mathematik', 'Chemie', 'Mathe', 'Doktoarbeit',
              'auslandssemester', 'Schulzeiten', 'Schulkarriere', 'abgesclossen', 'Schulzeit', 'Dozenten',
              'Sprachtest', 'Urkunde', 'studieren', 'Kommilitonen', 'Einstufungstest', 'Schuelern', 'Stiftung',
              ],
    'travel': ['Auto', 'Taxi'
                       'Fahrrad', 'Fahrradhelm', 'gefahren', 'Fahhrad', 'transportieren',
               'Bahn', 'Urlaub', 'LKW', 'Traktor', 'Zugfahrt', 'Fuehrerschein',
               'Autobahn', 'Radfahren', 'fahren', 'dozent',
               'Unfall', 'Wandern', 'Radtour', 'rad ', 'Bibliotehk',
               'verkehr', 'Straßenverkehr',
               'Geschwindigkeit', 'fahr', 'fuhr', 'fährt',
               'Reise', 'Bus', 'Skifahren', 'Snowboarden', 'Wanderung',
               'Fahrer', 'Fahrkarte',
               'Ampel', 'Zug', 'Handbremse',
               'Abfahrt', 'Rückfahrt', 'Rueckfahrt',
               'Parkplatz', 'Wagen',
               'Strasse', 'Straße', 'Flug', 'Fahrzeug', 'Führerschein', 'Fahrlehrer', 'führerscheinprüfung'
               ],
}

if __name__ == "__main__":
    import random
    from collections import Counter
    import pandas as pd

    data_paths = ['reg_focus_train_data.csv', 'reg_focus_holdout_data.csv']
    for data_path in data_paths:
        data = pd.read_csv('../data/' + data_path)
        lexicon_annotated_topic = []
        for text in data.text:
            new_label = []
            found_topics = []
            for label, words in topic_words.items():
                for word in words:
                    if word.lower() in str(text).lower():
                        found_topics.append(word)
                        new_label.append(label)

            label_counter = Counter(new_label)
            if not new_label:
                lab = 'other'
                # if lab != annotated_label:
                #    print(text, found_topics)

            elif len(set(new_label)) == 1:
                lab = new_label[0]
            elif list(label_counter.values())[0] == list(label_counter.values())[1]:
                lab = random.choice(new_label)

            else:
                lab = label_counter.most_common(1)[0][0]
            lexicon_annotated_topic.append(lab)
        print(Counter(lexicon_annotated_topic))
        print()
        assert len(lexicon_annotated_topic) == len(data)
        data['lexicon_annotated_topic'] = lexicon_annotated_topic
        # print(list(data[data['lexicon_annotated_topic'] == 'other'].text))
        print(data.groupby(['focus', 'lexicon_annotated_topic']).size().reset_index().rename(columns={0: 'count'}))
        data.to_csv('../data/' + data_path.split('.')[0] + '_lex.csv')
