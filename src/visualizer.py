import streamlit as st
import os
import pickle
import pandas as pd
from pathlib import Path
from generate_explanations import examples
import streamlit.components.v1 as components
import matplotlib.pyplot as plt
import seaborn as sns

explanations_location = os.path.join(str(Path(__file__).parent.parent), 'explanations')
output_location = os.path.join(str(Path(__file__).parent.parent), 'output')

st.set_page_config(layout="wide")

experiments = ['', 'Compare Model Explanations', 'Plot training stats', 'Plot test results']

model_names = {'vanilla': 'vanilla',
               'masked_test_delete_False': 'masked_bert',
               'deleted_test_delete_False': 'deleted_bert',
               'masked_test_delete_True': 'masked_bert_masked_in_test',
               'deleted_test_delete_True': 'deleted_bert_deleted_in_test',
               'tfidf': 'tfidf_maxent',
               'tfidf_masked': 'masked_tfidf_maxent',
               'tfidf_deleted': 'deleted_tfidf_maxent',
               'adversarial_True_lexicon_label_0.2': 'lambda_0.2',
               'adversarial_True_lexicon_label_0.4': 'lambda_0.4',
               'adversarial_True_lexicon_label_0.6': 'lambda_0.6',
               'adversarial_True_lexicon_label_0.8': 'lambda_0.8',
               'adversarial_True_lexicon_label_1.0': 'lambda_1.0',
               'adversarial_False_lexicon_label_None': 'multi_task',
               'adversarial_True_lexicon_label_None': 'lambda_variable',
               }
model_names_revers = {y: x for x, y in model_names.items()}
mode_groups = {'adversarial': [m for m in model_names if 'adversarial' in m],
               'max_ent': [m for m in model_names if 'tfidf' in m],
               'bert_models': [m for m in model_names if 'tfidf' not in m]}

models = os.listdir(explanations_location)


@st.cache
def get_output():
    with open(output_location + '/result_03_25.pkl', 'rb') as f:
        aggregate_result = pickle.load(f)
    return aggregate_result


@st.cache
def get_sentence_id_mapping(examples):
    sentence_id_mapping = {}
    for ex_group, focus_dict in examples.items():
        for focus, samples in focus_dict.items():
            for i, ex in enumerate(samples):
                sentence_id_mapping[ex] = '_'.join([focus, str(i), ex_group])
    return sentence_id_mapping


def plot_stats(training_stats):
    fig, (ax1, ax2) = plt.subplots(1, 2)
    pd.set_option('precision', 2)
    df_stats = pd.DataFrame(data=training_stats)
    df_stats = df_stats.set_index('epoch')
    sns.set(style='darkgrid')
    sns.set(font_scale=1.5)
    plt.rcParams["figure.figsize"] = (20, 6)
    plt.setp((ax1, ax2), xticks=[1, 2, 3, 4])
    # LOSS
    ax1.plot(df_stats['Training Loss'], 'b-o', label="Training loss")
    ax1.plot(df_stats['Topic Training Loss'], 'k-o', label="Topic training loss")
    ax1.plot(df_stats['Topic Valid. Loss'], 'r-o', label="Topic Validation loss")
    ax1.plot(df_stats['Valid. Loss'], 'g-o', label="Validation loss")
    # ax1.title("Training & Validation Loss")
    # ax1.ylabel("Loss")
    ax1.legend()

    # F1 score
    ax2.plot(df_stats['Valid. F1.'], 'b-o', label="Topic F1.")
    ax2.plot(df_stats['Topic Valid. F1.'], 'g-o', label="RFT F1.")
    # ax2.title("RFT & Topic Validation F1_macro")
    # ax2.ylabel("Accuracy")

    plt.xlabel("Epoch")
    ax2.legend()
    return fig


aggregate_result = get_output()
sentence_id_mapping = get_sentence_id_mapping(examples=examples)

### start app outline
selected_exp = st.sidebar.selectbox('Choose an experiment', experiments, key='exp')

if selected_exp == 'Compare Model Explanations':
    selected_models = st.multiselect('Select models to be explained', sorted([model_names[m] for m in models]))
    selected_example_file = sentence_id_mapping[
        st.selectbox('Select sentence', list(sentence_id_mapping.keys()), key='example')]
    explain_topic = False
    if st.checkbox('show topic explanations'):
        explain_topic = True

    st.subheader("True label: " + selected_example_file.split('_')[0])

    for mod in selected_models:
        st.subheader("Model: " + mod)
        HtmlFile = open(os.path.join(explanations_location, model_names_revers[mod], selected_example_file + '.html'),
                        'r',
                        encoding='utf-8')
        source_code = HtmlFile.read()
        components.html(source_code, height=300, width=1000)
        if explain_topic and model_names_revers[mod] in mode_groups['adversarial']:
            st.write("Topic explanation")
            HtmlFile = open(
                os.path.join(explanations_location, model_names_revers[mod], selected_example_file + '_topic.html'),
                'r',
                encoding='utf-8')
            source_code = HtmlFile.read()
            components.html(source_code, height=300, width=1000)

elif selected_exp == 'Plot training stats':
    selected_model = st.multiselect('Select models to be explained', sorted([model_names[m] for m in models]))
    for mod in selected_model:
        stats = [agg.train_stats for agg in aggregate_result if
                 agg.ref == model_names_revers[mod] and 'train_stats' in vars(agg)]
        if stats:
            st.subheader(mod)
            st.write(plot_stats(stats[0]))

elif selected_exp == 'Plot test results':
    def update_label(x):
        return model_names[x]


    st.markdown("<h3 style='text-align: center;'>RFT classification fscore on test data </br></h3>",
                unsafe_allow_html=True)

    data = {vars(agg)['ref']: vars(agg)['f1_macro'] for agg in aggregate_result if agg.mode == "holdout"}
    data = pd.DataFrame(data.items())
    data.columns = ['model', 'fscore']
    # data['colors'] = data.model.apply(getcolor)
    data['model_name'] = data.model.apply(update_label)
    data = data.sort_values('fscore')
    fig, ax = plt.subplots()
    data.plot(kind='barh', x='model_name', y='fscore', ax=ax, figsize=(8, 7)).legend(loc='center left',
                                                                                     bbox_to_anchor=(1.5, 0.5))
    for i, v in enumerate(sorted(data.fscore)):
        plt.text(v + 0.05, i, str(round(v, 3)), color='steelblue', va="center")
    st.write(fig)
    st.markdown("<h3 style='text-align: center;'>Topic vs. RFT fscore in test data</br></h3>",
                unsafe_allow_html=True)
    data_all = [{"model": model_names[vars(agg)['ref']], "RFT_fscore": vars(agg)['f1_macro'],
                 "topic_fscore": vars(agg)['topic_f1_macro']}
                for agg in aggregate_result if agg.mode == "holdout" and "adversarial" in agg.ref]
    df = pd.DataFrame(data_all).set_index('model')
    df = df.sort_values('RFT_fscore')
    fig, ax = plt.subplots()
    df.plot.barh(ax=ax, figsize=(8, 7)).legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    st.write(fig)

    st.write(examples)