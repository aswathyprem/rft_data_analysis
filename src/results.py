from sklearn.metrics import classification_report, precision_recall_fscore_support, accuracy_score
# import matplotlib
# matplotlib.use("agg")
import seaborn as sns
import matplotlib.pyplot as plt


class EvalResult(object):
    def __init__(self, reference, input, y_true, y_pred, mode, **kwargs):

        if mode not in ['train-test', 'holdout']:
            raise Exception('Invalid mode passed')
        self.ref = reference
        self.mode = mode
        self.feats_counter = None
        if 'cross_val' in kwargs and mode == 'train-test':
            self.classification_report = None
            self.accuracy, self.precision, self.recall, self.f1_macro = kwargs['cross_val']
        elif 'train_stats' in kwargs and mode == 'train-test':
            self.train_stats = kwargs['train_stats']
        else:
            self.classification_report = classification_report(y_true=y_true, y_pred=y_pred)
            self.precision, self.recall, self.f1_macro, _ = precision_recall_fscore_support(y_true, y_pred,
                                                                                            average='macro')
            self.accuracy = accuracy_score(y_true=y_true, y_pred=y_pred)
            print(self.classification_report)
        if 'comment' in kwargs:
            self.comment = kwargs['comment']
        if 'meta' in kwargs:
            self.meta = kwargs['meta']
        if list(y_true) and list(y_pred):
            self.false_pred, self.true_pred = self.false_predictions(input, y_true, y_pred)

        if 'topic_pred' in kwargs and 'topic_true' in kwargs:
            self.topic_pred = kwargs['topic_pred']
            self.topic_true = kwargs['topic_true']
            if len(self.topic_pred) > 0:
                self.topic_classification_report = classification_report(y_true=self.topic_true, y_pred=self.topic_pred)
                self.topic_precision, self.topic_recall, self.topic_f1_macro, _ = precision_recall_fscore_support(
                    self.topic_true,
                    self.topic_pred,
                    average='macro')
                self.topic_accuracy = accuracy_score(y_true=self.topic_true, y_pred=self.topic_pred)

        if 'lime_features' in kwargs:
            self.feats_counter = kwargs['lime_features']

    def add_lime_features(self, x):
        self.feats_counter = x

    def false_predictions(self, inputs, y_true, y_pred):
        true_pred = []
        false_pred = []
        for text, gold_label, predicted_label in zip(inputs, y_true, y_pred):
            if predicted_label != gold_label:
                false_pred.append({'text': text, 'predicted': predicted_label, 'true': gold_label})
            else:
                true_pred.append({'text': text, 'predicted': predicted_label, 'true': gold_label})
        return false_pred, true_pred

    @staticmethod
    def plot_features(w):
        y = [count for tag, count in w.most_common(30)]
        x = [tag for tag, count in w.most_common(30)]
        plt.bar(x, y, color='crimson')
        plt.title("Term frequencies")
        plt.ylabel("Frequency")
        # plt.yscale('log') # optionally set a log scale for the y-axis
        plt.xticks(rotation=90)
        for i, (tag, count) in enumerate(w.most_common(20)):
            plt.text(i, count, f' {count} ', rotation=90,
                     ha='center', va='top' if i < 10 else 'bottom', color='white' if i < 10 else 'black')
        plt.xlim(-0.6, len(x) - 0.4)  # optionally set tighter x lims
        plt.tight_layout()  # change the whitespace such that all labels fit nicely
        plt.show()
        return plt
