import os
from pathlib import Path
from data_loader import DataLoader as dl
from sklearn.metrics import classification_report, plot_confusion_matrix
import numpy as np
from sklearn import linear_model, model_selection
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from results import EvalResult
import pandas as pd
import de_core_news_sm
import matplotlib

matplotlib.use("agg")
import matplotlib.pyplot as plt
from sklearn.pipeline import make_pipeline
from lime.lime_text import LimeTextExplainer
from random import randrange
from collections import Counter

data_location = os.path.join(str(Path(__file__).parent.parent), 'data')
output_location = os.path.join(str(Path(__file__).parent.parent), 'output')

german_stop_words = stopwords.words('german')
alternate_umlaut_stops = ['moegt', 'zurueck', 'muesst', 'fuenftes', 'wuerde', 'koennen', 'wuerden', 'grossen', 'ueber',
                          'waere', 'haette', 'dermassen', 'fuer', 'fuenf', 'demgemaess', 'moeglich', 'muessen',
                          'waehrend', 'ueberhaupt', 'darueber', 'spaeter', 'grosse', 'grosses', 'haetten', 'dass',
                          'waehrenddessen', 'muss', 'koennte', 'duerft', 'natuerlich', 'zunaechst', 'frueher',
                          'fuenften', 'heisst', 'moegen', 'dafuer', 'grosser', 'fuenfter', 'demgemaess',
                          'moechte', 'ausserdem', 'ausser', 'uebrigens', 'demgegenueber', 'gegenueber', 'koennt',
                          'duerfen', 'waehrenddem', 'fuenfte', 'gross', 'wurde', 'konnte', 'mal', 'beim', 'wäre', 'nie',
                          'immer', 'mehr', 'schon', 'gut', 'gute', 'kein', 'keins', 'haette', 'waere', 'sei', 'hätte',
                          'haette']

german_stop_words.extend(alternate_umlaut_stops)

nlp = de_core_news_sm.load()


class WB_models(object):

    def __init__(self):
        d = dl()
        self.train_data = d.train_data
        self.holdout_data = d.holdout_data
        self.train_data_LIWC = d.get_filtered_data(d.training_data_LIWC_path)
        self.holdout_data_LIWC = d.get_filtered_data(d.holdout_data_LIWC_path)
        self.train_100W_score = d.get_filtered_data(d.score_100W_training_data_path)
        self.holdout_100W_score = d.get_filtered_data(d.score_100W_holdout_data_path)
        self.label_dic = {i: list(set(self.train_data[self.train_data.label == i]['focus']))[0] for i in [0, 1]}
        self.ref = None

    def get_lexicon_classification(self, ref):
        self.ref = ref
        results = []
        predicted_label = []
        data = None
        if ref == 'LIWC_lexicon':
            data = self.holdout_data_LIWC
            data['promotion_focus'] = data['reward']
            data['prevention_focus'] = data['risk']
        elif ref == '100W_lexicon':
            data = self.holdout_100W_score
            data['promotion_focus'] = data['reward']
            data['prevention_focus'] = data['risk']
        gold_label = list(data.focus)
        for row in data.iterrows():
            if row[1]["prevention_focus"] < row[1]["promotion_focus"]:
                predicted_label.append('promotion')
            else:
                predicted_label.append('prevention')

        results.append(EvalResult(self.ref, input=list(data.text), y_true=gold_label,
                                  y_pred=predicted_label,
                                  mode='holdout'))
        return results

    def get_best_hyperparams(self, X, y):
        scaler = StandardScaler()
        log_reg = linear_model.LogisticRegression()
        pipe = Pipeline(steps=[('scaler', scaler),
                               ('log_reg', log_reg)])
        C = np.logspace(-4, 4, 50)
        solvers = ['newton-cg', 'lbfgs']
        max_iter = [1000]
        parameters = dict(log_reg__C=C, log_reg__solver=solvers, log_reg__max_iter=max_iter)
        gs_clf = GridSearchCV(pipe, parameters, verbose=1)
        gs_clf.fit(X, y)
        return gs_clf.best_estimator_.get_params()['log_reg__C'], gs_clf.best_estimator_.get_params()[
            'log_reg__solver']

    def apply_logistic_regression_relative_frequency(self, ref):
        self.ref = ref
        holdout_data = None
        train_data = None
        features = None
        if 'LIWC' in ref:
            train_data = self.train_data_LIWC
            holdout_data = self.holdout_data_LIWC
            features = ['risk', 'reward']
        elif '100W' in ref:
            train_data = self.train_100W_score
            holdout_data = self.holdout_100W_score
            features = ['risk', 'reward']
        X = train_data[features]
        y = train_data['label']
        X_val, y_val = holdout_data[features], holdout_data['label']
        return self.apply_logistic_regression(X, y, X_val, y_val, self.ref)

    def apply_logistic_regeression_with_tfidf_features(self, ref='TFIDF', delete=False):
        self.ref = ref
        tfidfconverter = TfidfVectorizer(max_features=1000, stop_words=german_stop_words, ngram_range=(1, 2), max_df=.7)
        if delete:
            self.ref = self.ref + '_delete'
            train_X = self.train_data.deleted
            val_X = self.holdout_data.deleted
        else:
            train_X = self.train_data.text
            val_X = self.holdout_data.text
        X = tfidfconverter.fit_transform(train_X).toarray()
        X_val = tfidfconverter.transform(val_X).toarray()
        print(tfidfconverter.get_feature_names())
        y = list(self.train_data.label)
        y_val = list(self.holdout_data.label)
        print("logistic regression with TFIDF features on hold_out data")
        result = self.apply_logistic_regression(X, y, X_val, y_val, self.ref)

        return result

    def apply_logistic_regression(self, X, y, X_test, y_test, ref_name):
        self.ref = ref_name
        results = []
        # create model
        clf = linear_model.LogisticRegression(max_iter=5000, random_state=42, solver='lbfgs')

        # run cross validation training data
        cross_val = StratifiedKFold(n_splits=10, random_state=100, shuffle=True)
        result = model_selection.cross_validate(clf, X, y, cv=cross_val,
                                                scoring=['accuracy', 'precision', 'recall', 'f1_macro'])
        cross_val_score = [np.mean(result['test_accuracy']), np.mean(result['test_precision']),
                           np.mean(result['test_recall']),
                           np.mean(result['test_f1_macro'])]
        results.append(
            EvalResult(ref_name, input=list(self.train_data.text), y_true=[], y_pred=[], mode='train-test',
                       cross_val=cross_val_score,
                       comment='crossvalidation'))
        # fit model in training data
        clf.fit(X, y)
        # predict test data
        y_pred = clf.predict(X_test)

        results.append(EvalResult(ref_name,
                                  input=list(self.holdout_data.text),
                                  y_true=list(y_test),
                                  y_pred=y_pred,
                                  mode='holdout'))
        return results
