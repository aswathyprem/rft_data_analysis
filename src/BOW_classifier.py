import os
import pickle
import numpy as np
from pathlib import Path
from topic_words import topic_words
from results import EvalResult
import de_core_news_sm
from spacy.attrs import ORTH
from data_loader import DataLoader as dl
from nltk.corpus import stopwords
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import linear_model, model_selection

nlp = de_core_news_sm.load()
nlp.tokenizer.add_special_case('<TOPIC_MASK>', [{ORTH: "<TOPIC_MASK>"}])
output_location = os.path.join(str(Path(__file__).parent.parent), 'output')
# model_location = os.path.join(str(Path(__file__).parent.parent), 'models')
model_location = '/mount/arbeitsdaten/tcl/Users/aswathy/models/'
german_stop_words = stopwords.words('german')
alternate_umlaut_stops = ['moegt', 'zurueck', 'muesst', 'fuenftes', 'wuerde', 'koennen', 'wuerden', 'grossen', 'ueber',
                          'waere', 'haette', 'dermassen', 'fuer', 'fuenf', 'demgemaess', 'moeglich', 'muessen',
                          'waehrend', 'ueberhaupt', 'darueber', 'spaeter', 'grosse', 'grosses', 'haetten', 'dass',
                          'waehrenddessen', 'muss', 'koennte', 'duerft', 'natuerlich', 'zunaechst', 'frueher',
                          'fuenften', 'heisst', 'moegen', 'dafuer', 'grosser', 'fuenfter', 'demgemaess',
                          'moechte', 'ausserdem', 'ausser', 'uebrigens', 'demgegenueber', 'gegenueber', 'koennt',
                          'duerfen', 'waehrenddem', 'fuenfte', 'gross', 'wurde', 'konnte', 'mal', 'beim', 'wäre', 'nie',
                          'immer', 'mehr', 'schon', 'gut', 'gute', 'kein', 'keins', 'haette', 'waere']

german_stop_words.extend(alternate_umlaut_stops)


class BOWClassifier(object):

    def __init__(self, model_name, train_data, holdout_data, x_col, y_col1, ref=None, **kwargs):

        self.masked = kwargs['masked'] if 'masked' in kwargs else False
        self.deleted = kwargs['deleted'] if 'deleted' in kwargs else False
        self.mask_topic_in_test_data = kwargs[
            'mask_topic_in_test_data'] if 'mask_topic_in_test_data' in kwargs else False
        self.delete_topic_in_test_data = kwargs[
            'delete_topic_in_test_data'] if 'delete_topic_in_test_data' in kwargs else False

        self.meta = kwargs
        self.ref = ref
        self.text_col = x_col
        self.label_col = y_col1
        self.model_name = model_name  # "bert-base-german-cased"
        self.train_data = train_data
        self.holdout_data = holdout_data

        self.labels = sorted(list(set(train_data[self.label_col])))

        if self.masked:
            print("==============TOPIC MASKED BOW=============")
            if not self.mask_topic_in_test_data:
                self.holdout_data['masked'] = self.holdout_data['text']
            self.text_col = 'masked'
        if self.deleted:
            print("==============TOPIC DELETED BOW=============")
            assert not self.masked
            if not self.delete_topic_in_test_data:
                self.holdout_data['deleted'] = self.holdout_data['text']
            self.text_col = 'deleted'

            # params
        self.output_model_dir = os.path.join(str(Path(__file__).parent.parent), 'models')
        # self.train_data, self.val_data = train_test_split(self.train_data, test_size=0.1)

    def apply_logistic_regeression(self):
        results = []
        tfidfconverter = TfidfVectorizer(stop_words=german_stop_words, ngram_range=(1, 2))
        X = tfidfconverter.fit_transform(self.train_data[self.text_col]).toarray()
        y = list(self.train_data[self.label_col])

        #print(tfidfconverter.get_feature_names())

        # create model
        clf = linear_model.LogisticRegression(max_iter=5000, random_state=42, solver='lbfgs')

        # run cross validation training data
        cross_val = StratifiedKFold(n_splits=10, random_state=42, shuffle=True)

        cross_val_res = model_selection.cross_validate(clf, X, y, cv=cross_val,
                                                       scoring=['accuracy', 'precision', 'recall', 'f1_macro'])
        cross_val_score = [np.mean(cross_val_res['test_accuracy']), np.mean(cross_val_res['test_precision']),
                           np.mean(cross_val_res['test_recall']),
                           np.mean(cross_val_res['test_f1_macro'])]
        results.append(
            EvalResult(self.ref, input=list(self.train_data[self.text_col]), y_true=[], y_pred=[], mode='train-test',
                       cross_val=cross_val_score,
                       comment='crossvalidation'))
        print(cross_val_score)
        # fit model in training data
        clf.fit(X, y)

        # predict test data
        X_test = tfidfconverter.transform(self.holdout_data[self.text_col]).toarray()
        y_test = list(self.holdout_data[self.label_col])
        y_pred = clf.predict(X_test)
        result = EvalResult(self.ref,
                            input=list(self.holdout_data[self.text_col]),
                            y_true=y_test,
                            y_pred=y_pred,
                            mode='holdout',
                            meta=self.meta)
        print(result.classification_report)
        results.append(result)

        # save model as pickle
        model_path = model_location + '/_' + self.ref + '.pkl'
        with open(model_path, 'wb') as file:
            pickle.dump(clf, file)
        vectorizer_path = model_location + '/_' + self.ref + '_vectorizer' + '.pkl'
        with open(vectorizer_path, 'wb') as file:
            pickle.dump(tfidfconverter, file)
        return results


if __name__ == "__main__":
    dl = dl()
    inputs = {"model_name": "bert-base-german-cased",
              "train_data": dl.get_training_data(),
              "holdout_data": dl.get_holdout_data(),
              "x_col": "text",
              "y_col1": "label",
              "ref": "tfidf"
              }
    clf = BOWClassifier(**inputs)
    res = clf.apply_logistic_regeression()
    inputs['masked'] = True
    clf = BOWClassifier(**inputs)
    res = clf.apply_logistic_regeression()
    inputs['masked'] = False
    inputs['deleted'] = True
    clf = BOWClassifier(**inputs)
    res = clf.apply_logistic_regeression()
