import os
import pandas as pd
import numpy as np
from bert4seqclassification_modified import BertForSequenceClassification as AdversarialBertForSequenceClassification
from transformers import BertForSequenceClassification
from transformers import BertTokenizerFast
import torch
from lime.lime_text import LimeTextExplainer
import pickle
from pathlib import Path
from sklearn.pipeline import make_pipeline

holdout_data_path = os.path.join(str(Path(__file__).parent.parent), 'data', 'reg_focus_holdout_data_lex.csv')
model_path = '/mount/arbeitsdaten/tcl/Users/aswathy/models'
# model_path = os.path.join(str(Path(__file__).parent.parent), 'models')
explanation_path = os.path.join(str(Path(__file__).parent.parent), 'del_explanations')
output_path = os.path.join(str(Path(__file__).parent.parent), 'output')

data = pd.read_csv(holdout_data_path)
print(data)
#examples = {"holdout": {"promotion": [text.rstrip() for text, label in zip(data.text, data.focus) if
#                                      label == 'promotion' and len(text.split()) > 15][:10],
#                        "prevention": [text.rstrip() for text, label in zip(data.text, data.focus) if
#                                       label == 'prevention' and len(text.split()) > 15][:10]
#                        },
#            "manual": {'promotion': ['Ich bin recht zuversichtlich, dass ich das Studium schaffe.',
#                                     'Die Prüfung kannst du dir locker zutrauen.',
#                                     'Da Studium ist auch dazu da, dass man sich weiterentwickelt.',
#                                     'Mit dem Auto habe ich mir einen Traum verwirklicht.',
#                                     'Ich fühlte mich ziemlich selbstsicher als ich mit dem Bus fuhr.',
#                                     'Ich sehnte mich nach meinem Freund.',
#                                     'Mich reizt die Fahrt an den Gardasee sehr',
#                                     'Mit einem schnellen Auto kann ich bei den Mädchen punkten.'],
#
#                       'prevention': ['Ich war ziemlich ängstlich vor der Prüfung.',
#                                      'Der Aufwand während des Studiums war enorm und kaum zu schaffen.',
#                                      'Im Studium musste ich viele Prüfungen ablegen, vor denen ich eine große Aversion hatte.',
#                                      'Es ist ziemlich bedenklich wie du dein Auto fährst.',
#                                      'Du solltest etwas besonnener mit deinem Auto fahren.',
#                                      'Ich bin echt besorgt wie du mit deinem Auto unterwegs bist, nimm doch besser den Bus.',
#                                      'Ich brauchte heute morgen so lange, so dass ich fürchtete, den Bus zu verpassen.',
#                                      'In dieser Zeit hat mir mein Freund sehr gefehlt.',
#                                      'Ich konnte mit dem Auto nicht weiterfahren weil die Straße gesperrt war.',
#                                      'Mein Freund hindert mich an meiner Entwicklung.']
#                       }
#            }

examples = {"del" : {"promotion": ["Das Gefuehl hat mir das bestehen des vermittelt. Insbesondere, als ich bei der muendlichen in Bio eine 1 ehalten habe. Ich dachte damals, auf dem richtigen Weg zu sein. Egal was noch passiert, das hatte ich schon mal geschafft",
                                   "Es gab eine Zeit im , die mir dieses Gefuehl gegeben hat. Nicht andauernd, aber doch immer zeitweise. Das gefuehl auf der Stelle zu treten und nicht voran zu kommen. Und ob das  ueberhaupt die richtige Entscheidung fuer mich war.",
                                   "Mit dem xxx  habe ich mir einen Traum verwirklicht."],
                     "prevention": ["Der Aufwand während des xxx war enorm und kaum zu schaffen.",
                                    "Vor einer laengeren war ich zunaechst sehr besorgt ueber den Zustand des, insbesondere da mehrere, mir zum Teil nicht bekannte Personen mitfuhren. Also habe ich das am Tag zuvor auf alle mir relevant erscheinenden moeglichen technischen Maengel untersucht, und wie sich herausstellte, waren einige Dinge tatsaechlich reparaturbeduerftig und die noetige Sicherheit waere nicht gegeben gewesen."]}}


class BertExplainer:
    def __init__(self):
        self.class_names = ['promotion', 'prevention']
        self.explainer = LimeTextExplainer(class_names=self.class_names)
        self.model = None
        self.tokenizer = None
        self.adversarial = False
        self.topic_class_names = ['other', 'study', 'travel']
        self.topic_explainer = LimeTextExplainer(class_names=self.topic_class_names)

    def get_logits(self, sent):
        encoded_dict = self.tokenizer.encode_plus(
            sent,
            add_special_tokens=True,
            max_length=420,
            truncation=True,
            padding='longest',
            return_attention_mask=True,
            return_tensors='pt',
        )
        self.model.cuda()
        input_ids = encoded_dict["input_ids"].detach().clone().to('cuda')
        attention_mask = encoded_dict["attention_mask"].detach().clone().to('cuda')
        output = self.model(input_ids, attention_mask)
        return (output.logits, output.topic_logits) if self.adversarial else (output.logits,)

    def predict_proba(self, samples):
        probs = []
        for i, sent in enumerate(samples):
            softmax = torch.nn.functional.softmax(self.get_logits(sent)[0], dim=1)
            probability = softmax.tolist()[0]
            probs.append(probability)
        print(len(probs))
        return np.array(probs)

    def predict_topic_proba(self, samples):
        probs = []
        for i, sent in enumerate(samples):
            softmax = torch.nn.functional.softmax(self.get_logits(sent)[1], dim=1)
            probability = softmax.tolist()[0]
            probs.append(probability)
        return np.array(probs)

    def generate_explanations(self, model_name, example_group):
        if 'adversarial' in model_name:
            self.adversarial = True
        if '_' + model_name in os.listdir(model_path):
            model_folder = model_path + '/_' + model_name
            if self.adversarial:
                self.model = AdversarialBertForSequenceClassification.from_pretrained(model_folder,
                                                                                      num_topic_labels=len(
                                                                                          self.topic_class_names))
            else:
                self.model = BertForSequenceClassification.from_pretrained(model_folder)
            self.tokenizer = BertTokenizerFast.from_pretrained(model_folder)
            if not os.path.exists(explanation_path + '/' + model_name):
                os.makedirs(explanation_path + '/' + model_name)
            for focus, sents in examples[example_group].items():
                for i, sent in enumerate(sents):
                    print(sent)
                    fname = explanation_path + '/' + model_name + '/' + "_".join(
                        [focus, str(i), example_group]) + '.html'
                    exp = self.explainer.explain_instance(sent, self.predict_proba)  # , num_samples=)
                    exp.save_to_file(fname, text=True)
                    if self.adversarial:
                        topic_fname = explanation_path + '/' + model_name + '/' + "_".join(
                            [focus, str(i), example_group + '_topic']) + '.html'
                        topic_exp = self.topic_explainer.explain_instance(sent,
                                                                          self.predict_topic_proba)  # , num_samples=)
                        topic_exp.save_to_file(topic_fname, text=True)


class LogregExplainer():
    def __init__(self):
        self.class_names = ['promotion', 'prevention']
        self.explainer = LimeTextExplainer(class_names=self.class_names)
        self.model = None
        self.vectorizer = None

    def generate_explanation(self, model_pkl, example_group):
        with open(model_path + '/_' + model_name + '.pkl', 'rb') as f:
            self.model = pickle.load(f)
        with open(model_path + '/_' + model_name + '_vectorizer' + '.pkl', 'rb') as f:
            self.vectorizer = pickle.load(f)
        m = make_pipeline(self.vectorizer, self.model)
        if not os.path.exists(explanation_path + '/' + model_name):
            os.makedirs(explanation_path + '/' + model_name)
        for focus, sents in examples[example_group].items():
            for i, sent in enumerate(sents):
                print(sent)
                fname = explanation_path + '/' + model_name + '/' + "_".join([focus, str(i), example_group]) + '.html'
                exp = self.explainer.explain_instance(sent, m.predict_proba)  # , num_samples=)
                exp.save_to_file(fname, text=True)


if __name__ == "__main__":
    with open(output_path + '/result_03_23.pkl', 'rb') as f:
        aggregate_result = pickle.load(f)

    model_names = list(set([agg.ref for agg in aggregate_result]))
    print(model_names)
    bert_exp = BertExplainer()
    log_exp = LogregExplainer()
    for example_group in examples.keys():
        for model_name in model_names:
            print(model_name)
            if 'tfidf' not in model_name and 'deleted' in model_name:
                bert_exp.generate_explanations(model_name, example_group)
            else:
                pass
                log_exp.generate_explanation(model_name, example_group)
